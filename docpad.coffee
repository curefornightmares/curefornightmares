# For a the complete config specification, see https://docpad.org/docs/config

docpadConfig = {

	localeCode: "en-US"

	plugins:
		cleanurls:
			static: true

		marked:
			markedOptions:
				gfm: true
				sanitize: false
				smartLists: true
				smartypants: true
				tables: true

	collections:
		# Build a collection of the "meta" (about) pages.
		meta: ->
			@getCollection( "html" ).findAllLive({ layout: "meta" })

		# Build a collection of the archive list pages.
		archive: ->
			@getCollection( "html" ).findAllLive({ layout: "archive" }, [{ menu_order: 1 }])

		# Build a collection for all stories, photos, and sketches.
		story: ->
			@getCollection( "html" ).findAllLive({ layout: "story" })

		# Build a collection for the short stories series.
		short_stories: ->
			@getCollection( "html" ).findAllLive({ series: "short stories" })

		photography: ->
			@getCollection( "html" ).findAllLive({ series: "photography" })

		sketches: ->
			@getCollection( "html" ).findAllLive({ series: "sketches" })

		old: ->
			@getCollection( "html" ).findAllLive({ series: "old" })

		# Build a collection of all stories/picture posts, sorted by reverse chronological order.
		chronological: ->
			@getCollection( "html" ).findAllLive({ layout: "story" }, [{ date: -1 }])

	templateData:
		site:
			title: "Cure For Nightmares"
			tagline: "We are alive whether we like it or not — so why not?"
			domain: "https://curefornightmares.com"

		breadcrumbLink: ->
			if @document.series is "short stories"
				return "/alphabetically#short-stories"
			else if @document.series is "photography"
				return "/alphabetically#photography"
			else if @document.series is "sketches"
				return "/alphabetically#sketches"
			else if @document.series is "old"
				return "/alphabetically#earliest-attempts"

		niccolo:
			email: "niccolo@curefornightmares.com"

		andrew:
			email: "andrew@tosk.in"
			web: "https://andrew.tosk.in"

		collaborators:
			hikaru:
				name: "hikaru starr"
				web:
					main: "https://plus.google.com/+hikarustarr/"
					secondary: "https://instagram.com/hikarustarr/"
				rel: "author friend"

		coauthorsList: ->
			if @document.coauthors
				return "Niccolo Florence, #{@document.coauthors}"
			else return "Niccolo Florence"

	environments:
		# Default DocPad settings for `development` is fine, but may change later.
		# development:

		production:
			# cache time limit in milliseconds, sent to clients
			maxAge: 345600000
			port: 80

	env: "development"
}

# Export the DocPad Configuration.
module.exports = docpadConfig
