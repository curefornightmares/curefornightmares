You should never put your only copy of anything remotely important in the
`public/` directory, because everything in here gets deleted just before
rebuilding the website. That includes this file.

YOU HAVE BEEN WARNED 😉.
