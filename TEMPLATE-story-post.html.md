---
title: ""
coauthors: "" # comma-separated, all one string
date: "" # YYYY-MM-DDtHH:MM
layout: "story"
series: ""
license: "cc-share-alike"
blurb: "" # remember: manually convert to smart punctuation here.

# Building the complete URL for every image.
host:
	toskin: "" # the containing directory at media.tosk.in (needs a trailing slash)
	archive: "" # the containing directory at <subdomain>.archive.org (needs a trailing slash)
base_name: "" # something like "lowercase-hyphen-separated--"
mediatype: "" # probably .jpg

# Comic script for posts with text embedded in the imagery.
comic_script:
	# Note the hyphen in panel/part labels; comics have more regular names, so this matches part of the file URL.
	panel-1: "" # remember to convert to smart punctuation here.
	panel-2: ""
	panel-3: ""

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	sketch_1:
		base_name: ""
		alt: "" # remember to convert smart punctuation here.
		caption: "" # remember to convert smart punctuation here.
	photo_1:
		base_name: ""
		alt: ""
		caption: ""

# Where users can download source files.
source:
	flickr: ""
	internet_archive: ""

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	"FULL AUTHOR NAME":
		url: ""
		works:
			"FULL TITLE": "URL" # convert smart punctuation here
			"FULL TITLE": "URL"
	"FULL AUTHOR NAME":
		url: ""
		works:
			"FULL TITLE": "URL"
			"FULL TITLE": "URL"
---

<!-- text content goes here -->
