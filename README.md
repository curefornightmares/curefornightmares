# CureForNightmares.com

Source files for building the website at curefornightmares.com. Source files get
processed/rendered using [Pug](https://pugjs.org) and
[Stylus](http://stylus-lang.com).

Unless noted otherwise (at the bottom of each rendered page or in the "license"
property of the CSON frontmatter), all works here are published under the Creative
Commons Attribution-ShareAlike 4.0 International License. Meaning basically that
you're *free* to copy, share, and remix these works, for any purpose, as long as
you "share alike." See the Creative Commons summary and full license for
details, either [online](https://creativecommons.org/licenses/by-sa/4.0/) or
here in the `LICENSE.md` file. Reposts and remixes of any of these stories
should attribute Niccolo Florence and link to <http://curefornightmares.com>.



## Works I borrowed

### fonts

* Alice, by [Cyreal](http://cyreal.org)
* Quicksand, by [Andrew Paglinawan](http://andrewpaglinawan.com/)
* Source Code Pro, by [Paul D. Hunt](https://github.com/adobe-fonts/source-code-pro)

### graphics

* "All User Inputs" is a combination of lightly edited images from Open Clip Art,
so it and the source images are released to the public domain.
	* [Multitouch - tap](https://openclipart.org/detail/175324/multitouch-tap) --- by williamtheaker / Jonathan Averstedt
	* [Keyboard Keys](https://openclipart.org/detail/27549/keyboard-keys) --- by Simanek
	* [mouse](https://openclipart.org/detail/211818/mouse) --- by sixsixfive
* My "magnifying glass" image is based on a magnifying glass by
[sheikh_tuhin, at OpenClipart](https://openclipart.org/detail/78163/officeglassmagnify).
Both versions are public domain.

Rights to other icons and logos, such as those used for social
networking links, obviously belong to their respective owners.
