---
title: "Outage"
date: "2015-03-23t12:00"
layout: "story"
series: "short stories"
license: "cc-share-alike"
blurb: "Days like this make me realize how the last few members of the Donner Party must have felt."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/outage/"
	archive: "https://ia801508.us.archive.org/25/items/Outage--by-Niccolo-Florence/"
base_name: "outage--"
mediatype: ".jpg"

# Comic script goes in the image alt text.
comic_script:
	panel-1: "Panel 1: We’re in an apartment living room. It’s dark, inside and out. I’ve just\ncome home, I’m stepping in through the front door. My roommate sits on the\ncouch, candles set up on the coffee table, reading a book by flashlight.\n\n	Caption:\n		Outage\n		by Niccolo Florence\n		curefornightmares.com"
	panel-2: "Panel 2: I try the light switch, and the lights turn on.\n\n	SOUND FX\n		*flick*\n\n	NICCOLO\n		What are you doing? The power isn’t out.\n\n	ROOMMATE\n		No. It’s the internet."
	panel-3: "Panel 3: The coffee table has been replaced with a steel barrel full of burning\nwood. My roommate and I stand, wrapped in blankets, hunched over the fire.\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157651457838096/"
	archive: "https://archive.org/details/Outage--by-Niccolo-Florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	Wonderlane:
		url: "https://www.flickr.com/photos/wonderlane/"
		works:
			"Cream Living room with maroon": "https://www.flickr.com/photos/wonderlane/3224308182/"
	Seattle Municipal Archives:
		url: "https://www.flickr.com/people/24256351@N04"
		works:
			"Couple in living room, circa 1930s": "https://commons.wikimedia.org/wiki/File:Couple_in_living_room,_circa_1930s.jpg"
	Ratha Grimes:
		url: "https://www.flickr.com/photos/ratha/"
		works:
			"Burn barrel": "https://www.flickr.com/photos/ratha/15547086994"
---
