---
title: "Seems Legit"
date: "2015-02-18t11:41"
layout: "story"
series: "short stories"
license: "cc-share-alike"
blurb: "Phishing for phishers: To catch a semi-literate criminal, you have to think like a semi-literate criminal."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/seems-legit/"
	archive: "https://ia601501.us.archive.org/24/items/Seems-Legit--by-Niccolo-Florence/"
base_name: "seems-legit--"
mediatype: ".jpg"

# Comic script goes in the image alt text.
comic_script:
	part-1: "Panel 1: A woman’s hands, with long red fingernails, resting on a keyboard.\n\n		Seems Legit\n		by Niccolo Florence\n		curefornightmares.com\n\nPanel 2: Computer screen capture of someone clicking “Send” on their email\nclient.\n\nPanel 3: Sidelong view of me, sitting at my desk, reading an email."
	part-2: "Panel 4: Onscreen now we see the actual email I’m reading.\n\n		> Good day.\n\n		I have found your resume placed on one of the job sites. So I have\n		a great vacancy for you.\n\n		Duties:\n		Providing backing in completing transactions with clients.\n\n		Qualification:\n		* Over 22 years of age\n		* United States nationality\n		* An ability of keeping in touch with the manager during the whole\n		  working day\n		* An ability devoting at least 2 - 4 hours a day to your work\n		* Punctuality\n\n		Compensation:\n		The agent’s earnings combines the fixed monthly salary of 2.3k USD\n		and the fee of 5percents for every successfully performed payment.\n		The proximate yearly salary of each Agent will be up to $51000.\n\n		You do not have to possess any special qualification for this job.\n		I do not doubt that any responsible person complying with the above\n		mentioned requirements will perform this job quite effectively.\n		If you have interest in cooperation with us please e-mail back, and\n		we’ll reach you in the short term to offer some subsidiary\n		description.\n\nPanel 5: Me, considering, rubbing my chin, thoughts and pull-quotes floating over\nmy head…\n\n		No mention of the organization she represents…\n		Job pays 50k a year…\n		but requires virtually no qualifications…\n		Found me “on one of the job sites” …\n\nPanel 6: Me, writing a response email.\n\n		> I am do not really need American dollar jobs at this currency time.\n		How ever I mayb e have a great oppertunity for you!\n\n		You see the laws in my county are so legally in my countr I must be\n		moving current around. I will send a large some of my currency dollars\n		into your bank account and then you will wire it back to my\n		Switserland bank account If you help me withthis I will let you\n		keep$10000 US dollars of the money as constipation for your helping\n		me. It should be ver easy let me know if your intrested and I will\n		explane the details in detail.\n\n		Sincerly,\n\n		Olanrewaju Afolayan,\n		Crowned Prince of Nigeria\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157650903571722"
	archive: "https://archive.org/details/Seems-Legit--by-Niccolo-Florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	Adikos:
		url: "https://www.flickr.com/photos/adikos"
		works:
			"Female Typing": "https://www.flickr.com/photos/adikos/4440682278"
	Luke Jones:
		url: "https://www.flickr.com/photos/befuddledsenses/"
		works:
			"CRT Recreation": "https://www.flickr.com/photos/befuddledsenses/7786643438"
	Eric Schmuttenmaer:
		url: "https://www.flickr.com/photos/akeg/"
		works:
			"slworking edit": "https://www.flickr.com/photos/akeg/4056063907/"
---
