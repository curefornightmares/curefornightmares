---
title: "Umbilical"
date: "2014-07-27t15:20"
layout: "story"
series: "short stories"
license: "cc-share-alike"
blurb: "I have a love-hate relationship with technology."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/umbilical/"
	archive: "https://ia902308.us.archive.org/2/items/Terrycloth-Doll-Wire-Doll/"
base_name: "umbilical--"
mediatype: ".jpg"

# Comic script goes in the image alt text.
comic_script:
	full: "Panel 1 of 1: A single panel. The scene echoes Harry Harlow's experiments on rhesus\nmacaques, specifically the part with the terrycloth doll and the wire doll,\nas described in his 1958 paper “The Nature of Love.”\n(This was just one part of what I think makes up some of the cruelest and\nyet most fascinating series of experiments in science.\n&lt;http://psychclassics.yorku.ca/Harlow/love.htm&gt;)\nHere, we see a baby monkey sitting between two potential mother dolls —\none with an abstracted face and cold wire body but designed to hold a\nbottle of milk, the other having a more monkey-like face and a big eerie\nsmile, set atop a web server. The baby monkey faces the web server\n“mother,” and clings to a cable sticking out from behind the big machine.\n\n	Caption: Sometimes, I start to resent technology, much the way I’m sure a fetus\n	resents its umbilical cord.\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157645541315737"
	archive: "https://archive.org/details/Terrycloth-Doll-Wire-Doll"
---
