---
title: "a Wet Spot In the Desert, Already Drying"
date: "2014-08-02t22:54"
layout: "story"
series: "photography"
license: "cc-share-alike"
blurb: "It’s pretty dry here in Northern Nevada; even when it’s raining, the humidity often hovers around 30% or less. But that doesn’t stop neighbors from growing orange trees, or the golf course no one ever visits from overwatering the grass."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/photography/a-wet-spot-in-the-desert/"
	archive: "https://ia802302.us.archive.org/7/items/A-Wet-Spot-In-the-Desert-Already-Drying--by-Andrew-Toskin/"
base_name: "a-wet-spot-in-the-desert--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	photo_1:
		base_name: "2014-08-01--10-22-59"
		alt: "Emily sits in a red wagon among the Nevadan scrub and pinyon pine in her front yard, soaking wet. She looks up at the sky, puzzled."
	photo_2:
		base_name: "2014-08-01--10-30-58"
		alt: "Emily turns her confusion onto us, looking directly into the camera."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157646180984463/"
	archive: "https://archive.org/details/A-Wet-Spot-In-the-Desert-Already-Drying--by-Andrew-Toskin"
---

Some days it feels like you're the only one getting rained on.
