---
title: "Font of Wisdom"
date: "2014-09-06t13:27"
layout: "story"
series: "photography"
license: "cc-share-alike"
blurb: "We all <em>wish</em> my sister would hide her light under a bushel."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/photography/font-of-wisdom/"
	archive: "https://ia902303.us.archive.org/24/items/Font-of-Wisdom--by-Andrew-Toskin/"
# In this case, `base_name` is used only for generating the path to the thumbnail in archive lists.
base_name: "font-of-wisdom--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	photo_1:
		base_name: "2014-08-30--20-25-45"
		alt: "Emily sits cross-legged on the ground, outside, at night, holding a\nflashlight high over her head. Grapevines grow to her right, and a wooden\npallet is propped up to her left. She wears a tie-dye shirt, but looks\noddly regal, serene, with the light washing out her face, and looping\narcs of light springing from the top of her head."
	photo_2:
		base_name: "2014-08-30--20-27-29"
		alt: "Emily leans forward, more aggressive, propped up on her left arm, her\nother arm still lifting the flashlight. Looping arcs of light still\nspring from her head."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157646897242128"
	archive: "https://archive.org/details/Font-of-Wisdom--by-Andrew-Toskin"
---

See the complete set at
[Flickr](https://www.flickr.com/photos/andrewtoskin/sets/72157646897242128).
