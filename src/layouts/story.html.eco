<%- @partial( "header" ) %>

<nav>
	<%- @partial( "nav-previous-next" ) %>
</nav>

<article>
	<header>
		<h1><%- @document.title %></h1>

		<p class="publication-date">
			<time datetime="<%- @moment( @document.date ).format( 'YYYY-MM-DD' ) %>">
				<%- @moment( @document.date).format( "MMMM D, YYYY" ) %>
			</time>
		</p>
	</header>

	<%- @content %>

	<% if @document.comic_script: %>
	<% for panel of @document.comic_script: %>
	<figure>
		<img src="<%= @document.host.toskin + @document.base_name + panel + '-sd' + @document.mediatype %>"
			srcset="<%= @document.host.toskin + @document.base_name + panel + '-sd' + @document.mediatype %> 1125w,
				<%= @document.host.toskin + @document.base_name + panel + '-hd' + @document.mediatype %> 2250w,
				<%= @document.host.toskin + @document.base_name + panel + '-uhd' + @document.mediatype %> 4500w"
			alt="" />
	</figure>
	<% end %>
	<!-- Transcript of the comic. -->
	<p>
		<a id="toggle-transcript">
			Transcript
		</a>
	</p>

	<section id="transcript">
		<h2>Full comic script</h2>
		<% for panel of @document.comic_script: %>
		<p><%- @document.comic_script[ panel ] %></p>
		<% end %>
	</section>
	<% end %>

	<% if @document.picture_post: %>
	<% for image of @document.picture_post: %>
	<figure>
		<img src="<%= @document.host.toskin + @document.picture_post[ image ].base_name + '-sd' + @document.mediatype %>"
			srcset="<%= @document.host.toskin + @document.picture_post[ image ].base_name + '-sd' + @document.mediatype %> 1125w,
				<%= @document.host.toskin + @document.picture_post[ image ].base_name + '-hd' + @document.mediatype %> 2250w,
				<%= @document.host.toskin + @document.picture_post[ image ].base_name + '-uhd' + @document.mediatype %> 4500w"
			alt="" />
		<% if @document.picture_post[ image ].caption: %>
		<figcaption>
			<%- @document.picture_post[ image ].caption %>
		</figcaption>
		<% end %>
	</figure>
	<% end %>
	<!-- Transcript / semantics of the artwork. -->
	<p>
		<a id="toggle-transcript" );">
			Show alt text
		</a>
	</p>

	<section id="transcript">
		<h2>Transcript / semantics of the artwork</h2>
		<% for image of @document.picture_post: %>
		<p><%- @document.picture_post[ image ].alt %></p>
		<% end %>
	</section>
	<% end %>

	<section class="source">
		<%- @partial( @document.license ) %>

		<p>
			You can get the full-size JPEGs
			<a href="<%- @document.source.flickr %>" rel="source">from Flickr</a>,
			and the complete source files and exported images
			<a href="<%- @document.source.internet_archive %>" rel="source">from the Internet Archive</a>.
		</p>

		<% if @document.attributions: %>
		<br />
		<p>Works I borrowed:</p>

		<ul>
			<% for author of @document.attributions: %>
			<li>
				<a href="<%- @document.attributions[ author ].url %>" xmlns:dct="http://purl.org/dc/terms/" rel="dct:source cite">
					<%- author %>
				</a>
				<ul>
					<% for title of @document.attributions[ author ].works: %>
					<li>
						<a href="<%- @document.attributions[ author ].works[ title ] %>" rel="cite">
							<%- title %>
						</a>
					</li>
					<% end %>
				</ul>
			</li>
			<% end %>
		</ul>
		<% end %>
	</section>

	<nav>
		<%- @partial( "nav-previous-next", { navRepeating: true }) %>
	</nav>

</article>

<%- @partial( "footer" ) %>
