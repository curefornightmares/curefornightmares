<% unless @navRepeating: %>
<script>
	var previousURL, nextURL;
</script>
<% end %>

<ul class="nav-previous-next">
	<!-- Get the current post: from there, we will be able to get the next and previous. -->
	<% for story, storyIndex in @getCollection( "chronological" ).toJSON(): %>
		<% if @document.id is story.id: %>
			<!-- Check if there even is a previous post first, then link to it. -->
			<% if storyIndex < @getCollection( "chronological" ).length - 1: %>
			<% previousStory = @getCollection( "chronological" ).toJSON()[ storyIndex + 1 ] %>
				<% unless @navRepeating: %>
				<li class="previous-edge">
					<a href="<%= previousStory.url %>" title="previously: <%- previousStory.title %>.">
						<img src="/media/turn-the-page-button.png" alt="previous" />
					</a>
				</li>

				<script>
					previousURL = "<%= previousStory.url %>";
				</script>
				<% end %>

			<li class="previous">
				<a href="<%= previousStory.url %>" title="previously: <%- previousStory.title %>.">
					←&nbsp;previous
				</a>
			</li>
			<% end %>

			<!-- Check if there even is a next page first, then link to it. -->
			<% if storyIndex >= 1: %>
			<% nextStory = @getCollection( "chronological" ).toJSON()[ storyIndex - 1 ] %>
				<% unless @navRepeating: %>
				<li class="next-edge">
					<a href="<%= nextStory.url %>" title="next: <%- nextStory.title %>.">
						<img src="/media/turn-the-page-button.png" alt="next" />
					</a>
				</li>

				<script>
					nextURL = "<%= nextStory.url %>";
				</script>
				<% end %>

			<li class="next">
				<a href="<%= nextStory.url %>" title="next: <%- nextStory.title %>.">
					next&nbsp;→
				</a>
			</li>
			<% end %>
		<% end %>
	<% end %>

	<% unless @navRepeating: %>
	<!-- Toggle showing navigation shortcuts. -->
	<li id="shortcuts-menu">
		<a id="toggle-shortcuts">
			<img src="/icons/hamburger-menu.svg" alt="extras menu" />
		</a>
	</li>

	<li id="shortcuts-list">
		<p>
			Navigation shortcuts:
		</p>

		<figure>
			<img id="all-user-inputs" src="/icons/all-user-inputs.svg" alt="keyboard, mouse, and touch" />
		</figure>

		<dl>
			<dt>
				Touch / Mouse
			</dt>
			<dd>
				Tap/click on the right edge of the screen to go to the next story,
				and the left edge to go to the previous story.
			</dd>

			<dt>
				Keyboard
			</dt>
			<dd>
				<kbd title="up arrow key">↑</kbd>, <kbd title="down arrow key">↓</kbd>,
				<kbd title="left arrow key">←</kbd>, and <kbd title="right arrow key">→</kbd>
				scroll smoothly if nothing on the page is selected.<br />
				<kbd>Shift</kbd> makes you scroll even faster.<br />
				<kbd>N</kbd> takes you to the next story.<br />
				<kbd>P</kbd> takes you to the previous story.<br />
				<kbd>Escape</kbd> deselects everything.
			</dd>
		</dl>
	</li>
	<% end %>
</ul>
