<footer>
	<hr />

	<% if @document.layout isnt "story": %>
	<p>Unless noted otherwise, all the works at
		<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">
			Cure For Nightmares
		</span>
		are made by
		<a xmlns:cc="http://creativecommons.org/ns#" href="<%- @site.domain %>"
			property="cc:attributionName" rel="cc:attributionURL author me">
			Niccolo Florence</a>,

		and are published under a

		<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
			Creative Commons Attribution-ShareAlike 4.0 International License</a>.

		Meaning basically that you’re <em>free</em> to copy, share, and remix these works,
		for any purpose, as long as you credit me as the source, and “share alike.”
		<strong>Any reposts or remixes of these works should attribute Niccolo Florence,
		and link back to <%- @site.domain %>.</strong>
	</p>
	<% end %>

	<address>
		<p><a href="mailto:<%- @niccolo.email %>">Talk to me!</a></p>

		<div id="subscribe">
			<p>Subscribe!</p>

			<ul>
				<p>feeds/updates</p>
				<ul>
					<li>
						<a href="/feed/" title="RSS feed" rel="feed">
							<img src="/icons/rss.svg" alt="RSS" />
						</a>
					</li>
					<li>
						<a href="https://tinyletter.com/cure-for-nightmares" rel="sub" title="subscribe by email, with TinyLetter">
							<img src="/icons/tinyletter.svg" alt="TinyLetter" />
						</a>
					</li>
					<li>
						<a href="http://tapastic.com/series/cure-for-nightmares" rel="syndication"
							title="You can also read Cure For Nightmares on Tapastic.">
							<img src="/icons/tapastic.svg" alt="You can also subscribe to Cure For Nightmares at Tapastic.com" />
						</a>
					</li>
				</ul>

				<p>social/blogging</p>
				<ul>
					<li>
						<a href="https://plus.google.com/+AndrewToskin" rel="me" title="circle me on Google+">
							<img src="/icons/google+.svg" alt="Google+" />
						</a>
					</li>
					<li>
						<a href="https://www.facebook.com/andrew.toskin" rel="me" title="friend me on Facebook">
							<img src="/icons/facebook.svg" alt="Facebook" />
						</a>
					</li>
					<li>
						<a href="https://diasp.eu/u/andrewtoskin" rel="me" title="add me to an aspect on Diaspora">
							<img src="/icons/diaspora.svg" alt="diaspora" />
						</a>
					</li>
					<li>
						<a href="https://foxcoloredtrees.wordpress.com" rel="me" title="follow me on WordPress.com">
							<img src="/icons/wordpress.svg" alt="WordPress.com" />
						</a>
					</li>
					<li>
						<a href="https://foxcoloredtrees.tumblr.com" rel="me" title="follow me on Tumblr">
							<img src="/icons/tumblr.svg" alt="Tumblr" />
						</a>
					</li>
				</ul>

				<p>works-in-progress and completed</p>
				<ul>
					<li>
						<a href="https://gitlab.com/terrycloth" rel="me" title="fork me on GitLab">
							<img src="/icons/gitlab.svg" alt="GitLab" />
						</a>
					</li>
					<li>
						<a href="https://github.com/terrycloth" rel="me" title="fork me on GitHub">
							<img src="/icons/github.svg" alt="GitHub" />
						</a>
					</li>
					<li>
						<a href="https://flickr.com/photos/andrewtoskin/" rel="me" title="follow me on Flickr">
							<img src="/icons/flickr.svg" alt="Flickr" />
						</a>
					</li>
					<li>
						<a href="http://www.hitrecord.org/users/Niccolo+Niccolo" rel="me" title="remix me on hitRECord">
							<img src="/icons/hitrecord.svg" alt="hitRECord" />
						</a>
					</li>
				</ul>
			</ul>
		</div>
	</address>

	<p>Some comics I like:</p>

	<ul id="comics-i-like">
		<li>
			<a href="http://asofterworld.com/" title="A Softer World, by Joey Comeau and Emily Horne"
				rel="nofollow">
				A Softer World
			</a>
		</li>
		<li>
			<a href="http://scarygoround.com/" title="Bad Machinery, by John Allison"
				rel="nofollow">
				Bad Machinery
			</a>
		</li>
		<li>
			<a href="http://www.lunarbaboon.com/" title="Lunarbaboon, by Lunarbaboon"
				rel="nofollow">
				Lunarbaboon
			</a>
		</li>
		<li>
			<a href="http://tapastic.com/series/BEAR" title="Bear, by Bianca Pinheiro"
				rel="nofollow">
				Bear
			</a>
		</li>
		<li>
			<a href="http://tapastic.com/series/4" title="DaneMen, by David Daneman"
				rel="nofollow">
				DaneMen
			</a>
		</li>
		<li>
			<a href="http://tapastic.com/series/Fail-by-Error" title="Fail by Error, by Min"
				rel="nofollow">
				Fail by Error
			</a>
		</li>
		<li>
			<a href="http://campcomic.com/" title="Camp Weedonwantcha, by Katie Rice"
				rel="nofollow">
				Camp Weedonwantcha
			</a>
		</li>
		<li>
			<a href="http://sufficientlyremarkable.com/" title="Sufficiently Remarkable, by Maki Naro"
				rel="nofollow">
				Sufficiently Remarkable
			</a>
		</li>
		<li>
			<a href="http://existentialcomics.com/" title="Existential Comics"
				rel="nofollow">
				Existential Comics
			</a>
		</li>
	</ul>
</footer>

<%- @getBlock( "scripts" ).toHTML() %>

<script src="/scripts/ui.js"></script>

</body>
</html>
