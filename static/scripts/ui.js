"use strict";
/*
 * TABLE OF CONTENTS
 *
 * Random header images
 * Show/Hide
 * Page refresh
 * Smooth scrolling and navigation
 */


/* ====================================
	Set a random site header image
==================================== */

// Array of *partial* paths to banner images.
var banners = [
	"/banners/2008_0110GradParty0023",
	"/banners/city-lights-8--bottom",
	"/banners/city-lights-8--top",
	"/banners/city-lights-11--middle",
	"/banners/city-lights--bottom",
	"/banners/city-lights--lower",
	"/banners/city-lights--top"
];

// Grab the site header element.
var header = document.getElementById( "header-story-posts" ) || document.getElementById( "header-archive-meta" );

// Check display size/resolution.
var resolution;
if ( window.innerWidth >= 3600 || window.devicePixelRatio >= 3 ) {
	resolution = "-uhd";
}
else if ( window.innerWidth >= 1800 || window.devicePixelRatio >= 2 ) {
	resolution = "-hd";
}
else {
	resolution = "-sd";
}

// Pick a random image and assign it to the header background.
var bannerIndex = Math.floor( Math.random() * banners.length );
header.style.backgroundImage = "url('" + banners[ bannerIndex ] + resolution + ".jpg')";



/* ===================================================
	Hide and show UI information to declutter the page
=================================================== */

// Get all toggleable UI page elements.
var metaPageList = document.getElementById( "meta-list" );
var archiveSortList = document.getElementById( "archive-sort" );
var search = document.getElementById( "search" );
// `searchBar` will show/hide inside `search`, but we need it so we can focus it shen showing the search form.
var searchBar = document.getElementById( "search-bar" );
var transcript = document.getElementById( "transcript" );
var shortcutsList = document.getElementById( "shortcuts-list" );

// Add all toggleable page elements to a hideList.
var hideList = [];

hideList.push( metaPageList, archiveSortList, search );

// These elements don't appear on every page, so check before adding them.
if ( shortcutsList ) {
	hideList.push( shortcutsList );
}
if ( transcript ) {
	hideList.push( transcript );
}

// All toggleable UI elements start as hidden.
for ( var i in hideList ) {
	hideList[ i ].classList.add( "ui-hidden" );
}

// Toggle hidden status of UI page elements.
var toggleHidden = function ( element ) {
	for ( var i in hideList ) {
		// If we're on the given toggleable element, toggle it.
		if ( hideList[ i ] === element ) {
			// Styles for accessibly hiding UI elements are handled in `cure-for-nightmares.css`.
			element.classList.toggle( "ui-hidden" );

			// If we've hidden the toggleable, then deselect everything.
			if ( element.classList.contains( "ui-hidden" )) {
				window.getSelection().removeAllRanges();
			}
			// But if we're turning on search, focus the search bar.
			else if ( element === search ) {
				searchBar.focus();
			}
		}
		// Hide everything else.
		else {
			hideList[ i ].classList.add( "ui-hidden" );
		}
	}
};

// Get all the toggleHidden buttons/links.
var toggleMeta = document.getElementById( "toggle-meta" );
var toggleArchive = document.getElementById( "toggle-archive" );
var toggleSearch = document.getElementById( "toggle-search" );
var toggleSearchExtra = document.getElementById( "toggle-search-extra" );
var toggleTranscript = document.getElementById( "toggle-transcript" );
var toggleShortcuts = document.getElementById( "toggle-shortcuts" );

// Click events tie these elements to `toggleHidden()`.
toggleMeta.addEventListener( "click", function () {
	toggleHidden( metaPageList );
}, false );

toggleArchive.addEventListener( "click", function () {
	toggleHidden( archiveSortList );
}, false );

toggleSearch.addEventListener( "click", function () {
	toggleHidden( search );
}, false );

// Not all toggling links appear on every page...
if ( toggleSearchExtra ) {
	toggleSearchExtra.addEventListener( "click", function () {
		toggleHidden( search );
	}, false );
}

if ( toggleTranscript ) {
	toggleTranscript.addEventListener( "click", function () {
		toggleHidden( transcript );
	}, false );
}

if ( toggleShortcuts ) {
	toggleShortcuts.addEventListener( "click", function () {
		toggleHidden( shortcutsList );
	}, false );
}



/* ====================================
	Page refresh
==================================== */

var refresh = document.getElementById( "refresh" );

if ( refresh ) {
	refresh.addEventListener( "click", function () {
		document.location.reload( true );
	}, false );
}



/* ====================================
	Smooth scrolling and navigation
==================================== */

// Backward and forward links depend on the current page, so values are set there.
var previousURL, nextURL;

// `keyboard` keeps track of which keys are currently being pressed/released, as boolean values.
var keyboard = {
	// Modifier keys. Only Shift is currently used, the others we watch to make
	// sure we don't interfere with default browser or OS commands.
	ALT   : false,
	CTRL  : false,
	SUPER : false,
	SHIFT : false,
	// Keys which actually issue navigation commands.
	ESC   : false,
	DOWN  : false,
	UP    : false,
	LEFT  : false,
	RIGHT : false,
	N     : false,
	P     : false
};

document.addEventListener( "keydown", function ( event ) {

	if ( event.key === "Alt" || event.key === "AltGraph" || event.keyCode === 18 ) {
		keyboard.ALT = true;
	}
	if ( event.key === "Control" || event.keyCode === 17 ) {
		keyboard.CTRL = true;
	}
	if ( event.key === "Hyper" || event.key === "Meta" || event.key === "Super"
	  || event.key === "Hyper" || event.key === "OS" || event.keyCode === 91 || event.keyCode === 92 ) {
		keyboard.SUPER = true;
	}
	if ( event.key === "Shift" || event.keyCode === 16 ) {
		keyboard.SHIFT = true;
	}

	if ( event.key === "Escape" || event.key === "Esc" || event.keyCode === 27 ) {
		keyboard.ESC = true;
	}

	if ( event.key === "ArrowDown" || event.key === "Down" || event.keyCode === 40 ) {
		keyboard.DOWN = true;
	}
	else if ( event.key === "ArrowUp" || event.key === "Up" || event.keyCode === 38 ) {
		keyboard.UP = true;
	}

	if ( event.key === "ArrowRight" || event.key === "Right" || event.keyCode === 39 ) {
		keyboard.RIGHT = true;
	}
	else if ( event.key === "ArrowLeft" || event.key === "Left" || event.keyCode === 37 ) {
		keyboard.LEFT = true;
	}

	if ( event.key === "n" || event.keyCode === 78 ) {
		keyboard.N = true;
	}
	if ( event.key === "p" || event.keyCode === 80 ) {
		keyboard.P = true;
	}
}, {
	capture: true,
	passive: true
});

document.addEventListener( "keyup", function ( event ) {

	if ( event.key === "Alt" || event.key === "AltGraph" || event.keyCode === 18 ) {
		keyboard.ALT = false;
	}
	if ( event.key === "Control" || event.keyCode === 17 ) {
		keyboard.CTRL = false;
	}
	if ( event.key === "Hyper" || event.key === "Meta" || event.key === "Super"
	  || event.key === "Hyper" || event.key === "OS" || event.keyCode === 91 || event.keyCode === 92 ) {
		keyboard.SUPER = false;
	}
	if ( event.key === "Shift" || event.keyCode === 16 ) {
		keyboard.SHIFT = false;
	}

	if ( event.key === "Escape" || event.key === "Esc" || event.keyCode === 27 ) {
		keyboard.ESC = false;
	}

	if ( event.key === "ArrowDown" || event.key === "Down" || event.keyCode === 40 ) {
		keyboard.DOWN = false;
	}
	if ( event.key === "ArrowUp" || event.key === "Up" || event.keyCode === 38 ) {
		keyboard.UP = false;
	}

	if ( event.key === "ArrowRight" || event.key === "Right" || event.keyCode === 39 ) {
		keyboard.RIGHT = false;
	}
	if ( event.key === "ArrowLeft" || event.key === "Left" || event.keyCode === 37 ) {
		keyboard.LEFT = false;
	}

	if ( event.key === "n" || event.keyCode === 78 ) {
		keyboard.N = false;
	}
	if ( event.key === "p" || event.keyCode === 80 ) {
		keyboard.P = false;
	}
}, {
	capture: true,
	passive: true
});

// Default scrolling speed, in pixels-per-animation-frame.
var speed = 10;
// Boosted scrolling speed.
var superSpeed = 30;

// Loop that watches keyboard inputs and actually does the scrolling and
// navigation.
var scrollNav = function () {
	// Escape always works: Deselect everything and reset all keys.
	if ( keyboard.ESC ) {
		window.getSelection().removeAllRanges();
		document.activeElement.blur();
		keyboard.ALT    = false;
		keyboard.CTRL   = false;
		keyboard.SHIFT  = false;
		keyboard.SUPER  = false;
		keyboard.ESC    = false;
		keyboard.DOWN   = false;
		keyboard.UP     = false;
		keyboard.LEFT   = false;
		keyboard.RIGHT  = false;
		keyboard.N      = false;
		keyboard.P      = false;
	}
	// Skip our custom navigation if anything in the page has focus or selection,
	// or if we're holding any other modifier keys
	if ( document.getSelection().anchorNode !== null || document.activeElement !== document.body
	  || keyboard.ALT === true || keyboard.CTRL === true || keyboard.SUPER === true ) {
		window.requestAnimationFrame( scrollNav );
	}
	else {
		// Smoother page scrolling with the arrow keys. Faster when holding Shift key.
		if ( keyboard.SHIFT && keyboard.DOWN ) {
			window.scrollBy( 0, superSpeed );
		}
		else if ( keyboard.SHIFT && keyboard.UP ) {
			window.scrollBy( 0, -superSpeed );
		}
		else if ( keyboard.DOWN ) {
			window.scrollBy( 0, speed );
		}
		else if ( keyboard.UP ) {
			window.scrollBy( 0, -speed );
		}

		if ( keyboard.SHIFT && keyboard.RIGHT ) {
			window.scrollBy( superSpeed, 0 );
		}
		else if ( keyboard.SHIFT && keyboard.LEFT ) {
			window.scrollBy( -superSpeed, 0 );
		}
		else if ( keyboard.RIGHT ) {
			window.scrollBy( speed, 0 );
		}
		else if ( keyboard.LEFT ) {
			window.scrollBy( -speed, 0 );
		}

		// Go to next or previous story.
		if ( keyboard.N ) {
			if ( nextURL ) {
				window.location.href = nextURL;
			}
		}
		else if ( keyboard.P ) {
			if ( previousURL ) {
				window.location.href = previousURL;
			}
		}
		window.requestAnimationFrame( scrollNav );
	}
};

window.requestAnimationFrame( scrollNav );
